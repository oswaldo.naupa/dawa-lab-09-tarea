const mongoose = require('mongoose');

let Schema = mongoose.Schema;
const ComentarioShema = new Schema(
    {
        nombre_usuario: {
            type: String,
            required: true
        },
        comentario: {
            type: String,
            required: true
        },
        img_usuario: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);
const PeliculasSchema = new Schema(
  {
    titulo: {
      type: String,
      required: true,
      unique: true
    },
    descripcion: {
      type: String,
      required: true
    },
    image: {
      type: String,
      required: true
    },
    categoria: {
      type: String,
      required: true
    },
    tags: [String],
    comentarios: [ComentarioShema]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Peliculas", PeliculasSchema);