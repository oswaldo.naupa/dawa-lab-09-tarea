const express = require('express');
const Usuario = require('../models/usuario');
const router = express.Router();
const jwt = require("jsonwebtoken")



router.get('/login', function(req, res){
    let error = [];
    //error.push({ text: res.errortoken});
    res.render('login', {error,
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});

router.get('/register', function(req, res){
    let error = [];
    res.render('register', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/welcome', function(req, res){
    res.render('welcome')
});
router.post('/registrar',  async function(req, res) {
    let error = [];
    const { username, email, password, confirmPassword } = req.body;
    if (password != confirmPassword) {
        error.push({ text: "Contraseñas no coinciden." });
        res.render("register", {
            error,
            username,
            email,
            password,
            confirmPassword
        });
    } else {
        // Look for email coincidence
        const emailUser = await Usuario.findOne({ email: email });
        const nameUser = await Usuario.findOne({ nombre: username });
        if (emailUser) {
            error.push({ text: "El correo ya esta en uso." });
        }
        if (nameUser) {
            error.push({ text: "El usuario ya esta en uso." });
        }
        if(error.length >0 ){
            res.render("register", {
                error,
                username,
                email,
                password,
                confirmPassword
            });
        } else {
          // Saving a New User
          const newUser = new Usuario({ nombre:username, email:email, password:password });
          newUser.password = await newUser.encryptPassword(password);
          await newUser.save();
          res.redirect("/welcome");
        }
    }
    
});
router.post('/ingresar', async function(req, res) {
    let error = [];
    const { email, password } = req.body;
    const user = await Usuario.findOne({email: email});
    if (!user) {
        error.push({ text: "Usuario invalido." });
        res.render("login", {
            error,
            email,
            password
        });
    } else {
        const match = await user.matchPassword(password);
        if(match) {
            let token = jwt.sign({
                usuario: user
            }, process.env.SEED,{expiresIn: process.env.CADUCIDAD_TOKEN})
            res.cookie('token', token)
            //localStorage.getItem('token', token)
            //console.log(token)
            res.redirect('/')
            
        } else {
            error.push({ text: "Contraseña invalida." });
            res.render("login", {
                error,
                email,
                password
            });
        }
        
    }
});
router.get('/salir', (req, res) => {
    res.clearCookie("token");
    res.redirect("/");
  });
module.exports = router